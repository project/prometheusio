<?php

if (!function_exists('apc_add')) {
  function apc_add($key, $var, $ttl = 0) {
    return apcu_add($key, $var, $ttl);
  }

  function apc_store($key, $var, $ttl = 0) {
    return apcu_store($key, $var, $ttl);
  }

  function apc_fetch($key, &$success = NULL) {
    return apcu_fetch($key, $success);
  }

  function apc_cas($key, $old, $new) {
    return apcu_cas($key, $old, $new);
  }

  function apc_inc($key, $step = 1, &$success = NULL) {
    return apcu_inc($key, $step, $success);
  }

  function apc_clear_cache() {
    return apcu_clear_cache();
  }

  class APCIterator extends APCUIterator {
    public function __construct($cache = NULL, $search = NULL, $format = APC_ITER_ALL, $chunk_size = 100, $list = APC_LIST_ACTIVE) {
      parent::__construct($search, $format, $chunk_size, $list);
    }
  }
}
